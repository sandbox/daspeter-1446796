<?php
/**
 * @file
 * Installation and update functions.
 */

/**
 * Implements hook_requirements().
 *
 * @param string $phase
 *   Phase this hook is called.
 *
 * @return array
 *   The requirements.
 */
function geoip_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break at install time.
  $t = get_t();

  // Test for a valid GeoIP database.
  $requirements['geoip_database'] = array(
    'title' => $t('GeoIP Database'),
  );

  // Ensure the module file is load.
  module_load_include('module', 'geoip');
  $file = geoip_data_file();

  if (!$file || !file_exists($file)) {
    $requirements['geoip_database']['value'] = l($t('Missing'), 'admin/config/system/geoip');
    $requirements['geoip_database']['description'] = $t('The GeoIP database file is missing or not configured. Download the latest file at <a href="@url">MaxMind.com</a>.', array('@url' => 'http://www.maxmind.com/download/geoip/database/GeoIP.dat.gz'));
    $requirements['geoip_database']['severity'] = ($phase == 'runtime') ? REQUIREMENT_WARNING : REQUIREMENT_ERROR;
  }
  else {
    $mtime = filemtime($file);
    if ($mtime < strtotime('1 months ago')) {
      $requirements['geoip_database']['value'] = $t('Out of date');
      $requirements['geoip_database']['description'] = $t('The GeoIP database file is more than a month old. Download the latest file at <a href="@url">MaxMind.com</a>.', array('@url' => 'http://www.maxmind.com/download/geoip/database/GeoIP.dat.gz'));
      $requirements['geoip_database']['severity'] = REQUIREMENT_WARNING;
    }
    else {
      $requirements['geoip_database']['value'] = $t('Installed');
    }
  }

  return $requirements;
}
